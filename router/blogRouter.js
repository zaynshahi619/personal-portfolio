import { Router } from "express";
import { createBlog } from "../controller/blogController.js";



let blogRouter = Router()

blogRouter.route("/").post().get(createBlog)

export default blogRouter