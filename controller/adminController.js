import { Blog } from "../model/model.js";


export let createAdminBlog = async (req, res, next) => {
  try {

    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let today  = new Date();
    let now = today.toLocaleDateString("en-US", options)




    let links = req.files.map((value,index)=>{
        return `${value.filename}`
    }) 
    let image = links.join("")
    let data = req.body;
    data = {
        ...data,
        picture:image,
        date:now

    }
    let result = await Blog.create(data);
    
    res.redirect("/admin");
  } catch (error) {
    console.log(error.message)
    res.redirect("/admin");
  }
};

export let readAdminBlog = (req, res, next) => {
  res.render("admin");
};
