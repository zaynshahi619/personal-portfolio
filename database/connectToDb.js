import mongoose from "mongoose"
import { Blog } from "../model/model.js"

let connectToDb = async()=>{
    try {
        await mongoose.connect("mongodb://0.0.0.0:27017/blog")
        console.log("database is successfully connected")
        // Blog.create({
        //     topic:"database",
        //     title:"Types of Databases",
        //     text:"Relational Databases",
        //     author:"Manish"
        // })
    } catch (error) {
        console.log(error.message)
        
    }

}
export default connectToDb