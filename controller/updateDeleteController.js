import { Blog } from "../model/model.js"

export let showBlog = async(req,res,next) =>{
    try {
        let result = await Blog.find({})
        res.render("updateDel",{result:result})
    } catch (error) {
        console.log(error.message)
    }
} 

export let updateBlog = async(req,res,next) =>{
    try {
        
        
    } catch (error) {
        console.log(error.message)
    }
}

export let deleteBlog = async(req,res,next) =>{
    try {
        let id = req.params.deleteId
        console.log(id)

        let result = await Blog.findByIdAndDelete(id)
        res.redirect("/admin")
        
    } catch (error) {
        console.log(error.message)
    }
}