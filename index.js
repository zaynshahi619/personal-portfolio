import express from "express"
import connectToDb from "./database/connectToDb.js"
import blogRouter from "./router/blogRouter.js"
import bodyParser from "body-parser"
import hbs from "hbs"
import adminRouter from "./router/adminRouter.js"
import updateDeleteRouter from "./router/updateDelete.js"

let app = express()
let port = 8080
app.use(express.static("./public"))
app.use(bodyParser.urlencoded({extended:true}))
app.set("view engine","hbs")
hbs.registerPartials("./views/partials")
app.set("views","views")

app.listen(port,()=>{
    console.log(`our app is listening to port ${port}`)

})

connectToDb()

app.use("/",blogRouter)
app.use("/admin",adminRouter)
app.use("/admin",updateDeleteRouter)