import { Schema } from "mongoose";

let blogSchema = Schema({
    picture:String,
    topic:String,
    title:String,
    text:String,
    author:String,
    date:String
    
})

export default blogSchema