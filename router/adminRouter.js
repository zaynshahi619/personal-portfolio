import { Router } from "express";
import { createAdminBlog, readAdminBlog } from "../controller/adminController.js";
import upload from "../middleware/upload.js";

let adminRouter = Router()

// let createAdmin=(req,res,next)=>{
//     console.log(req.files)  //from middleware upload

//     let links = req.files.map((value,index)=>{
//         return `http://localhost:8000/${value.filename}`
//     }) 

//     res.json({
//         success:true,
//         message:"upload file successfully",
//         result:links
//     })
// }


adminRouter
.route("/")
.get(readAdminBlog)
.post(upload.array("image",5),createAdminBlog)

export default adminRouter