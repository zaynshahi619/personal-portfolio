import { Router } from "express";
import { deleteBlog, showBlog, updateBlog } from "../controller/updateDeleteController.js";



let updateDeleteRouter = Router()

updateDeleteRouter
.route("/update-delete")
.get(showBlog)

updateDeleteRouter
.route("/update/:updateId")
.patch(updateBlog)

updateDeleteRouter
.route("/delete/:deleteId")
.delete(deleteBlog)

export default updateDeleteRouter